If you are a dog lover, you may find yourself scrolling through social media pages for all those cute images, and amazing stories. Finding a happy rescue story is heart endearing, and makes you want a pet, or more pets. Yet, you know that if you go to the animal rescue, you will want them all. It is heartbreaking to see animals unhappy. But, you know that you can’t help them all.

There are many reasons why you may not be able to own a pet, even if you want to:

•	Living in a home where the landlord does not allow pets.

•	Cost of vet bills and dog food.

•	Allergies to pet fur.

•	Feel that living in an apartment, without the use of a yard, is not fair to a pet.

•	Too busy, as you work all the time and feel you would not be able to spare the time to care for a pet.

There are many more, and all quite valid. Whatever your reasons, the good news is that you do not have to adopt a pet in your home, to help Rescue animals.

Here's some other things you could be doing:

**VOLUNTEERING**

•	Spend time volunteering at an Animal Shelter or Rescue Organization. Both staff and animals will appreciate the extra hands. What exactly will you be volunteering for? Help is needed in many areas, and there’s bound to be something that you can do.

•	Walking Dogs.

•	Cleaning out kennels.

•	Grooming.

•	Administrative duties.

•	Assisting at promotional events for the Shelter, often used for raising funds or finding home for the dogs.

At least you get to socialize with likeminded people and you should find that you sleep well at nights, with a healthy tiredness.

**TRANSPORTATION**

•	Some Shelters may have a low traffic rate for the movement of dogs in their care due to [dog’s destructive behavior](https://petchatz.com/7-reasons-your-dog-has-destructive-behavior/). This is particularly so in rural areas. If they cannot move the animals on to new homes, then they have to close their doors to new arrivals. Or even worse, euthanize them. To alleviate this problem, they often ship  animals to Rescue Organisations within the city boundaries. These tend to have a faster turnover and are more able to find homes.


•	Transportation costs dig into funds that would be better spent on looking after the  dogs. If you can volunteer your vehicle, and also afford the gas from your own budget, then this is a well served job you could be doing. It is so important and could help the shelter greatly, and the animals too.


•	Another related job to taxiing, and with a happy conclusion, is to drive the lucky dogs to their new forever homes. That would put a smile on any dog lover's face.


•	Here’s a great resource that will give you insight into the workings of dog shelters. Learn how you can help as a volunteer.

**FOSTERING**

Not quite the same as adopting, because it will only be for short period of time. It is still a huge commitment on your part. You  may have an old, sick or unsociable dog in your own home, for a short while.

The idea is that a homeless dog will stay with you, until they find their its forever home. If it is a sick dog, you may also have to take it to vet appointments. Though you won’t have to pay the vet bill. If you like this idea of this, then here’s a few issues to ponder:

•	If you already have pets, will they be okay?

•	If you have a landlord, check with them first.

•	You need to check with other members of your family that they are happy to have a foster pet in your home.

•	If you foster a sick dog, will you have the time and resources to go make regular vet visits?

•	Can you afford to pay for the dog's food?

•	Are you happy to accept potential mess in your home, especially if you are fostering a poorly dog?

•	Fostering a pet may involve some training it, have you the patience for this?

•	Rescue dogs often attend promotional events, it helps to find them a new home. Do you have the time and resources to do this?

•	Often you need to assess a dog's behaviour in a home environment, can you take part in this?

Finally, and most importantly, will you cope emotionally when it’s time to say goodbye. At least you can have the peace of mind that he/she is going to their forever home. Plus, if all went well, you could consider helping another dog.

**DONATIONS**

Rescue centers and animal welfare organizations always need extra money. Looking after animals, especially so many, is an expensive business. Every donation, no matter how small, is greatly appreciated. If you don't have much money, then you could always consider donating other things instead. Different Shelters and Organisations will have different policies. It might we worth checking out before you take your donated items to them. You should be safe with most of these following suggestions:

•	Dog Food. Biscuits. Treats. Check what sort of brands they accept. 

•	Various dishes for the food. Check what they normally use, such as plastic, ceramic or metal.

•	Towels. These can sometimes be used ones, but nice and clean when you take them.

•	Leashes and chains of various sizes.

•	Animal toys. You can even check if they take clean stuffed toys and go on a collection round your neighborhood.

•	Dog brushes, and other grooming tools.

•	Bedding. Another one where you need to check out what they already use.

•	Cleaning supplies.

•	Newspapers.

Another means of donating is to check if they have an Amazon Wishlist. some Shelters do this, and you can even view it by order of importance. Amazon will post directly to the Shelter. Of course there are also the Thrift or Charity Stores, run by the Society for the Prevention of Cruelty to animals (SPCA). They take a wide range of contributions form the public. Popular items that they can sell on are: clothing, books, records CD’s and many more other items. By selling preloved items in heir stores, money raised after overheads, goes towards the running of the animal shelters.

**RAISING FUNDS**

Options for raising money, to help keep abandoned dogs alive.

•	It is a great resource for raising funds for animal welfare. It's a mobile app that helps users to raise funds by walking. There is prize money to win, which has been donated by corporate sponsorships. If you win a prize, the money goes to the animal welfare of your choice.

•	Raise money by working in one of their retail establishments.

•	If they're having an event, come up with a good idea to make a stall there. Ideas like, a game for the kids to play, selling something, baking cakes.

•	Have a yard sale of your unwanted household items. This is a great way to have a clear out of your home. Then donate the money to animal welfare.

•	Social Media

This is a good way to spread the word around, and highlight dogs in need. Go to your local animal shelter and, with there permission, take photographs of the dogs waiting for adoption. Find out more information about individual animals from the shelter, and post it all on social media.

If you’re good at social media networking, you can keep updating the photographs and continue to plug for a home for a particular dog. Perhaps design yourself a webpage, or put the photo’s on Instagram to get some followers.

Whichever way you choose to help, know that you could be saving a dog’s life. What better reward could there possibly be.

